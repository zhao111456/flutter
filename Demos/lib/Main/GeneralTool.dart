import 'package:flutter/widgets.dart';

class ScreenUtils {

  static double screenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static double screenHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static bool isPhoneNumberValid(String phoneNumber) {
    final RegExp phoneNumberRegExp = RegExp(
      r'^((13[0-9])|(14[0-9])|(15[0-9])|(16[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\d{8}$',
    );
    return phoneNumberRegExp.hasMatch(phoneNumber);
  }
}