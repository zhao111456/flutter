import 'package:demos/Home/Home.dart';
import 'package:demos/Order/Order.dart';
import 'package:demos/Mine/Mine.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/material/bottom_navigation_bar.dart';
import 'package:get/get.dart';

class Tabs extends StatefulWidget {
 // const Tabs({super.key});
  @override
  State<Tabs> createState() => _TabsState();
}

class _TabsState extends State<Tabs> {

  int _currentIndex = 0;

  List _pageList =[
    HomePage(),
    OrderPage(),
    MinePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: this._pageList[this._currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: this._currentIndex,
        onTap: (index){
          setState(() {
            this._currentIndex = index;
          });
        },
        type: BottomNavigationBarType.fixed,
        items:  [
          BottomNavigationBarItem(
            icon: _currentIndex == 0 ? Image.asset('images/tab_home_selected@2x.png',width: 28) : Image.asset('images/tab_home_normal@2x.png',width: 28),
            label:'首页',
          ),
          BottomNavigationBarItem(
            icon: _currentIndex == 1 ? Image.asset('images/tab_order_selected@2x.png',width: 28) : Image.asset('images/tab_order_normal@2x.png',width: 28),
            label: '朋友圈',
          ),
          BottomNavigationBarItem(
            icon: _currentIndex == 2 ? Image.asset('images/tab_mine_selected@2x.png',width: 28) : Image.asset('images/tab_mine_normal@3x.png',width: 28),
            label: '我的',
          ),
        ],
        selectedItemColor: Colors.red,
        unselectedItemColor: Colors.grey,
        selectedFontSize: 12,
        iconSize: 20,
      ),
    );
  }
}
