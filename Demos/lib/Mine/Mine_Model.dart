import 'dart:convert';
import 'package:flutter/material.dart';

class Mine_Model{
   Mine_Model_Name? name;
   Mine_Model_Picture? picture;
   String? email;
   String? phone;
   String? gender;
   String? nat;
   //构造函数
   Mine_Model({this.name,this.picture,this.email,this.phone,this.gender,this.nat});

   factory Mine_Model.fromJson(Map<String, dynamic> json) {
     return Mine_Model(
       name: Mine_Model_Name.fromJson(json['name']),
       picture: Mine_Model_Picture.fromJson(json['picture']),
       email: json['email'],
       phone: json['phone'],
       gender: json['gender'],
       nat: json['nat']
     );
   }

   // 数据转模型
  static List<Mine_Model> resolveDataFromReponse(String responseData){

    Map<String, dynamic> jsonList= json.decode(responseData);
    List<dynamic> results = jsonList['results'];
    List<Mine_Model> model = results.map((json) => Mine_Model.fromJson(json)).toList();
    return model;
  }
}

class Mine_Model_Name{

   String? title;
   String? first;

   Mine_Model_Name({this.title,this.first});

   factory Mine_Model_Name.fromJson(Map<String, dynamic> json) {
     return Mine_Model_Name(
       title: json['title'],
       first: json['first'],
     );
   }
}

class Mine_Model_Picture{

  String? large;
                     // 使用 required 确保必须提供参数
  Mine_Model_Picture({ this.large});

  factory Mine_Model_Picture.fromJson(Map<String, dynamic> json){
    return Mine_Model_Picture(
      large: json['large'],
    );
  }
}

// class Mine_Model_