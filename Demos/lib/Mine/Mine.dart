import 'package:demos/Mine/Mine_LIst.dart';
import 'package:flutter/material.dart';
import 'Mine_Model.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';

class MinePage extends StatefulWidget {
  @override
  State<MinePage> createState() => _MinePageState();
}

class _MinePageState extends State<MinePage> {

  static List<Mine_Model> model = []; //数据源
  @override
  void initState(){
    super.initState();
    httpFunctions();
  }

  void httpFunctions(){
    var url = Uri.parse('https://p.ipic.vip/hceuvd.json');
    http.get(url).then((value) {
      // json 解析
     model = Mine_Model.resolveDataFromReponse(value.body);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GridView.builder(
          itemCount: model.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10
          ),
          itemBuilder: (BuildContext context, int index){
            Mine_Model mo = model[index];
            return InkWell(
                child: Card(
                    child: CachedNetworkImage(
                      imageUrl:mo.picture!.large ?? '',
                      // placeholder: (context, url) => CircularProgressIndicator(),
                      // errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                ),
              onTap: (){
                  Get.to(MessageItem(mo, index));
              },
            );
          }
      ),
    );
  }
}
