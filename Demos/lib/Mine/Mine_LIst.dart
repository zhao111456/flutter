
import 'package:flutter/material.dart';
import 'package:demos/Mine/Mine_Model.dart';

class MessageItem extends StatelessWidget {

  final Mine_Model message;
  final int index;// 动画
  MessageItem(this.message,this.index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(message.name!.title ?? ''),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Container(
      //  child: Hero(
      //      tag: this.index ?? 0,
            child: Image.network(
              message.picture!.large ?? '',
              fit: BoxFit.fitWidth,
            ),
       // ),
        constraints: BoxConstraints.expand(),
      ),
    );
  }
}

