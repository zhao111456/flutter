import 'package:demos/Home/Home_SegmentedController.dart';
import 'package:demos/Mine/Mine.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final textController = TextEditingController(); //监听输入

  void saveSomething() async{
    final perfernce = await SharedPreferences.getInstance();
    perfernce.setString('keysss', this.textController.text);
    print('保存的内容是: ${textController.text}');
  }

  void getSomething(Function callback) async{
    final perfernce = await SharedPreferences.getInstance();
    callback(perfernce.getString('keysss'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: getSearchBar(),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            iconSize: 28,
            color: Colors.grey,
            onPressed: () {
               if(textController.text != '') {
                 saveSomething();
                 getSomething((string){
                   print('取除的内容是: ${string}');
                 });
               }
            }
          ),
        ],
      ),
      body: DefaultTabController(
        length: 3,
        child: Column(
          children: [
            Container(
              color: Colors.white,
              // child: Material(
              //  color: Colors.white,
                child: TabBar(
                  labelColor: Colors.black,
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: Colors.black,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicatorWeight: 3,
                  labelStyle: TextStyle(fontSize: 15),
                  tabs: [
                    Tab(text: '花季'),
                    Tab(text: '鉴定'),
                    Tab(text: '文章'),
                  ],
                ),
            //  ),
            ),
            Expanded(
              flex: 1,
              child: TabBarView(
                children: [
                  MinePage(),
                  SegmentedHome1(),
                  MinePage(),
                ],
              ),
            ),//剩余的其余部分
          ],
        ),
      ),
    );
  }

  // 搜索
  Widget getSearchBar(){
   return Container(
     height: 48,
     //设置边框
     decoration: BoxDecoration(
       borderRadius: BorderRadius.circular(24),
       border: Border.all(color: Colors.grey,width: 2),
     ),
     child: Row(
       crossAxisAlignment: CrossAxisAlignment.center,
       children: [
         SizedBox(width: 15),
         Icon(Icons.search,color: Colors.grey),
         Expanded(
             child: Container(
               alignment: Alignment.center,
               child: TextField(
                 controller: textController,
                 decoration: InputDecoration(
                   hintText:  '输入搜索内容',
                   border: InputBorder.none,
                 ),
                 textAlignVertical: TextAlignVertical.center,
               ),
             )
         ),
         new IconButton(// 清楚按钮
             icon: Icon(Icons.cancel),
             color: Colors.black87,
             onPressed: (){
                textController.clear();
             },
         ),
       ],
     ),
   );
  }


}

