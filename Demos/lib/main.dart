import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:oktoast/oktoast.dart';
import 'dart:io';
import 'dart:convert';
import 'Main/Tabbar.dart';
import 'Lunch/launch.dart';
import 'package:get/get.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return OKToast(
      child:GetMaterialApp(
        theme: ThemeData(
          scaffoldBackgroundColor: Color(0xFFF3F4F5),//背景颜色
          primarySwatch: Colors.grey,
          // primaryColor: Colors.white,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: new Launchpage(),//const MyHomePage(title: 'Flutter Demo Home Page'),
        initialRoute: '/',
        defaultTransition: Transition.rightToLeftWithFade,//结合GetX 使用
        builder: (context,child) => Scaffold( //全局点击空白页收起键盘
          body: GestureDetector(
            onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
            child: child,
          ),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

 class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
     // httpFunctions();
     // httpFunctions1();
    });
  }

  // 数据 http 请求
  httpFunctions(){
    var url = Uri.parse('https://randomuser.me/api/?results=30');
    http.get(url).then((value) {
      print('数据 ${value.body}');
    });
  }
  // dart 自带请求
  httpFunctions1() async {
    var url = Uri.parse('https://randomuser.me/api/?results=30');
    var httpClent = new HttpClient();
    var request = await httpClent.getUrl(url);
    var response = await request.close();
    var jsonString = await response.transform(utf8.decoder).join();
    print('数据 ${jsonString}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
