import 'package:demos/Main/GeneralTool.dart';
import 'package:flutter/cupertino.dart';
import 'package:demos/Mine/Mine_Model.dart';
import 'package:flutter/material.dart';
class OrderListCell extends StatelessWidget {

  final Mine_Model model;
  OrderListCell(this.model);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 5,
          vertical: 3
        ),
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,//居左
            children: [
               Row(
                 children: [
                   Padding(
                       padding: EdgeInsets.only(
                         top: 20.0,
                         left: 15,
                         right: 15,
                         bottom: 10
                       ),
                       child: ClipRRect(
                         child: Image.network(
                           model.picture!.large ?? '',
                           width: 50,
                           height: 50,
                         ),
                         borderRadius: BorderRadius.circular(25),
                       ),
                   ),
                   Padding(
                     padding: EdgeInsets.only(
                         top: 15.0,
                         left: 0,
                         right: 15,
                         bottom: 10
                     ),
                     child: Text(
                       model.name!.first ?? '',
                       style: TextStyle(
                         fontSize: 15,
                         color: Colors.black
                       ),
                     ),
                   ),
                   Padding(
                     padding: EdgeInsets.only(
                         top: 10.0,
                         left: 0,
                         right: 15,
                         bottom: 5
                     ),
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: [
                         Text(
                           'phone:  ' + model.phone!,
                           style: TextStyle(
                               fontSize: 13,
                               color: Colors.grey
                           ),
                         ),
                         Text(
                           'gender:  ' + model.gender!,
                           style: TextStyle(
                               fontSize: 13,
                               color: Colors.grey
                           ),
                         ),
                         Text(
                           'nat:  ' + model.nat!,
                           style: TextStyle(
                               fontSize: 13,
                               color: Colors.grey
                           ),
                         ),
                       ],
                     ),
                   )
                 ],
               ),
               Divider(),//分割线
               Row(
                 children: [
                    Padding(
                      padding: EdgeInsets.only(
                        top: 5,
                        left: 10,
                        right: 5,
                        bottom: 15
                      ),
                      child: Text(
                        'email:  ' +  model.email!
                      ),
                    ),
                   //这是为了撑开右边的箭头
                   Expanded(
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.end,
                       children: [
                         Padding(
                           padding: EdgeInsets.only(
                             bottom: 10
                           ),
                           child: Icon(
                             Icons.keyboard_arrow_right,
                             color: Colors.grey,
                           ),
                         ),
                       ],
                     ),
                   ),
                 ],
               )
              ],
          ),
        ),
    );
  }
}
