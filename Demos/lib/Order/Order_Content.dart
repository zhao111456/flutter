import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';
import 'package:demos/Main/GeneralTool.dart';

class Order_Content extends StatefulWidget {
  final String title;
  Order_Content(this.title);

  @override
  State<Order_Content> createState() => _Order_ContentState();
}

class _Order_ContentState extends State<Order_Content> {

  final textController = TextEditingController(); //监听输入
  final codeTextController = TextEditingController(); //监听输入
  bool isPressed = false;//按钮是否改变颜色
  bool isButtonEnabled = false;//按钮是否可以点击
  bool isLongButtonEnabled = false;//登录按钮是否可以点击
  int _secondsRemaining = 60;
  bool isShowSeconds = false;
  Timer? timer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(widget.title,style: TextStyle(
          fontSize: 18,
        ),),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 200),
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Container(
                height: 45, // 设置高度
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Row(
                  children: [
                    Padding(padding: EdgeInsets.only(left: 20),child:Text('+86',style: TextStyle(fontSize: 16))),
                    Padding(padding: EdgeInsets.only(left: 10,right: 10,bottom: 5),child:Text('|',style: TextStyle(fontSize: 23,color: Color(0xFFF3F4F5)))),
                    Expanded(
                      child:TextField(
                        controller: textController,
                        maxLines: 1,
                        maxLength: 11, // 设置最大字符数
                        cursorColor: Color(0xFF0D47A1),
                        decoration:InputDecoration(
                            border: InputBorder.none,
                            hintText: '请输入手机号',
                            hintStyle: TextStyle(color: Color(0xFFBDBDBD)), // 设置提示文本的颜色
                            counter: SizedBox.shrink(), // 不显示字符计数器
                            contentPadding: EdgeInsets.symmetric(vertical: 8.5)
                        ),
                        keyboardType: TextInputType.number,
                        onChanged: (text) {
                            setState(() {
                              if (text.length > 0){
                                isPressed = true;
                                isButtonEnabled = true;
                              }else{
                                isPressed = false;
                                isButtonEnabled = false;
                              }
                            });
                        },
                        textAlignVertical: TextAlignVertical.center,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Container(
                height: 45, // 设置高度
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 20),
                        child:TextField(
                          controller: codeTextController,
                          maxLines: 1,
                          maxLength: 6, // 设置最大字符数
                          cursorColor: Color(0xFF0D47A1),
                          keyboardType: TextInputType.number,
                          decoration:InputDecoration(
                              border: InputBorder.none,
                              hintText: '请输入验证码',
                              hintStyle: TextStyle(color: Color(0xFFBDBDBD)), // 设置提示文本的颜色
                              counter: SizedBox.shrink(), // 不显示字符计数器
                              contentPadding: EdgeInsets.symmetric(vertical: 9)
                          ),
                          textAlignVertical: TextAlignVertical.center,
                        ),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: TextButton(
                            onPressed: isButtonEnabled ? (){
                              sendMessgeCode();
                            } : null,
                            child: Text(
                                isShowSeconds ? '$_secondsRemaining S' : '获取验证码',
                                style: TextStyle(
                                  color: isPressed ? Colors.red : Colors.grey,
                                )
                            )
                        ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 50),
              padding: EdgeInsets.symmetric(horizontal: 30),
              height: 40,
              child: Align(
                child: Container(
                  width: double.infinity, // Make the container take up the available space
                  child: TextButton(
                    style: ButtonStyle(
                      minimumSize: MaterialStateProperty.all(Size(double.infinity, double.infinity)),
                      backgroundColor: isLongButtonEnabled ? MaterialStateProperty.all(Colors.red) : MaterialStateProperty.all(Colors.red.withOpacity(0.3)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0), // Adjust the radius for the desired roundness
                        ),
                      ),
                    ),
                    onPressed: isLongButtonEnabled? () {
                      loginSucc();
                    } : null,
                    child: Text(
                      '登录',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void sendMessgeCode(){
    if (ScreenUtils.isPhoneNumberValid(textController.text)){
      showToast('发送成功');
      setState(() {
        isShowSeconds = true;
        isLongButtonEnabled = true;
      });

      timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
        if (_secondsRemaining == 0) {
          timer.cancel(); // Stop the timer when countdown reaches 0
        } else {
          setState(() {
            _secondsRemaining--;
            if (_secondsRemaining == 0){
                isShowSeconds = false;
                _secondsRemaining = 60;
                timer.cancel();
            }
          });
        }
      });
    }else {
      showToast('请输入正确的手机号');
    }
  }

  void loginSucc(){

    if (ScreenUtils.isPhoneNumberValid(textController.text) == true && codeTextController.text.length == 6){
      showToast('登录成功');
      return;
    }
    if ( codeTextController.text.length != 6){
      showToast('请输入正确的验证码');
      return;
    }
    if (ScreenUtils.isPhoneNumberValid(textController.text) == false){
      showToast('请输入正确的手机号');
      return;
    }
    if (codeTextController.text.length<1){
      showToast('请输入验证码');
      return;
    }
  }
}
