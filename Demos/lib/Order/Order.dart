import 'package:demos/Order/Order_Lit_Cell.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:demos/Order/Order_Content.dart';
import 'package:demos/Mine/Mine_Model.dart';
import 'package:http/http.dart' as http;
import 'package:easy_refresh/easy_refresh.dart';

class OrderPage extends StatefulWidget {
  const OrderPage({super.key});

  @override
  State<OrderPage> createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {

  static List<Mine_Model> model = [];

  EasyRefreshController _controller = EasyRefreshController(
    controlFinishRefresh: true,
    controlFinishLoad: true,
  );

  @override
  void initState(){
    super.initState();
    httpFunctions();
  }

  void httpFunctions(){
    var url = Uri.parse('https://p.ipic.vip/hceuvd.json');
    http.get(url).then((value) {
      // json 解析
      model = Mine_Model.resolveDataFromReponse(value.body);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text('朋友圈',style:TextStyle(fontSize: 18)),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: EasyRefresh(
        controller: _controller,
        onRefresh: () async {
          _controller.finishRefresh();
          _controller.resetFooter();
        },
        onLoad: () async {
          // 加载更多回调，可以在这里执行加载更多操作
          _controller.finishLoad(IndicatorResult.noMore);
        },
        // header: MaterialHeader(), // 刷新头部样式
        // footer: MaterialFooter(), // 加载尾部样式
        child: ListView.builder(
          itemCount: model.length,
          itemBuilder: (context,index){
            return InkWell(// InkWell 是为了让它可点击
              child: OrderListCell(model[index]),
              onTap: (){
                Get.to(Order_Content(model[index].name!.first ?? ''));
              },
            );
          },
        ),
      ),


      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Get.to(Order_Content('老子跳了哈'));
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This
    );
  }
}
