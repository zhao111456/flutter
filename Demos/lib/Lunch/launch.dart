import 'package:flutter/material.dart';
import 'dart:async';
import '../Main/Tabbar.dart';
import 'package:get/get.dart';

class Launchpage extends StatefulWidget {
  //const Launchpage({super.key});
  @override
  State<Launchpage> createState() => _LaunchpageState();
}

class _LaunchpageState extends State<Launchpage> with AutomaticKeepAliveClientMixin{
  @override
  bool get wantKeepAlive => true;
  @override
  void initState(){
    super.initState();
    new Future.delayed(Duration(seconds: 3),(){
      Get.off(Tabs());
    });
  }

  Widget build(BuildContext context) {
    return new Container(
      child: Image.asset('images/startTips.png'),
    );
  }
}
